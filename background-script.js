browser.contextMenus.create({
    id: "linguee-button",
    title: "Linguee",
    contexts: ["selection"],
    command: "_execute_browser_action"
});